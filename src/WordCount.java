import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;


public class WordCount {
	public static void main(String[] args) throws IOException {
		String FILE_URL = "https://gist.githubusercontent.com/anonymous/0f0374e431bb88f403c9/raw/b8ad310a887093bca6bcc54192677c24bb00a7a2/AliceInWonderland.txt";
		URL url = new URL(FILE_URL);
		InputStream input = url.openStream();
		Scanner scanner = new Scanner(input);
		final String DELIMS = "[\\s,.\\?!\"():;]+";
		scanner.useDelimiter(DELIMS);
		WordCounter wordcount = new WordCounter();
		while(scanner.hasNext()){
			wordcount.addWord(scanner.next());
		}
		String[] wordlist = wordcount.getSortedWords();
		for(int i=0;i<20;i++){
			System.out.println(wordlist[i]+" : "+wordcount.getCount(wordlist[i]));
		}
	}
}
