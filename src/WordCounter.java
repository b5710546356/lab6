import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class WordCounter {

	Map wordMap;
	int wordcount;
	
	
	public WordCounter(){
		wordMap  = new HashMap<String,Integer>();
	}
	
	public void addWord(String word){
		word = word.toLowerCase();
		if(wordMap.get(word)==null){
			wordMap.put(word,1);
		}
		else{
			wordMap.put(word,(int)(wordMap.get(word))+1);
		}
	}
	
	public Set<String> getWords(){
		Set<String> wordSet = new HashSet<String>(wordMap.keySet());
		return wordSet;
	}
	
	public int getCount(String word){
		word = word.toLowerCase();
		return (int)(wordMap.get(word));
	}
	
	public String[] getSortedWords(){
		List wordList = new ArrayList(wordMap.keySet());
		Collections.sort(wordList);
		String []array = new String[wordList.size()];
		array = (String[])wordList.toArray(array);
		return array;
	}
}
